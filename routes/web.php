<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {	
    return view('welcome');
});
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('home', 'HomeController@index');
Route::get('2af', 'TwoAuthenticationFactorController@index')->name('twoaf');
Route::post('2af', 'TwoAuthenticationFactorController@check')->name('twoaf_validate');
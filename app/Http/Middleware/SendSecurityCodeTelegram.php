<?php

namespace App\Http\Middleware;

use DB;
use Closure;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SendSecurityCodeTelegram
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if (Auth::check()) {
            
            $checkSecurityCode = $this->checkSecurityCodeAlreadyUsed(Auth::user()->id);
            if (true == $checkSecurityCode) {
                return $next($request);
            }

            // create number random     
            $random = new \PragmaRX\Random\Random();
            $code = $random->numeric()->size(7)->get();

            // get user id user
            $user = User::where('email', Auth::user()->email)->first();

            DB::beginTransaction();

            try {

                $this->saveSecurityCode($user->id, $code);
                app('thirdparty_telegram_helper')->send(Auth::user()->chat_id_telegram, $code);
                DB::commit();                
                return redirect()->route('twoaf');

            } catch (\Exception $e) {

                DB::rollback();
                dd($e->getMessage());

            }
        }

        return redirect('auth/login');
    }

    public function checkSecurityCodeAlreadyUsed($userId)
    {        
        $securityCode = DB::table('security_codes')->where('user_id', $userId)->first();
        if ($securityCode) {
            return ($securityCode->is_used == 0) ? false : true;
        }
        return false;
    }

    public function saveSecurityCode($userId, $code)
    {
        $securityCode = DB::table('security_codes')->where('user_id', $userId)->count();

        if ($securityCode > 0)
        {
            DB::table('security_codes')->where('user_id', $userId)->update([
                'code' => $code,                
                'updated_at' => Carbon::now()
            ]);
        }
        else
        {
            DB::table('security_codes')->insert(
                [
                    'code' => $code, 'user_id' => $userId, 
                    'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
                ]
            );
        }
    }
}

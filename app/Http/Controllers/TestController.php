<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = 'ok';
        $destination = '297432696';
        $anu = Telegram::sendMessage([
            'chat_id' => $destination,
            'parse_mode' => 'HTML',
            'text' => '<b>' .$message .'</b>'
        ]);
        dd($anu);
    }
}

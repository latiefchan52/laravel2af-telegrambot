<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\ValidateRequestSecurityCodeTelegram;

class TwoAuthenticationFactorController extends Controller
{
    public function index(Request $request)
    {    	
    	return view('auth.twoaf');
    }

    public function check(ValidateRequestSecurityCodeTelegram $request)
    {
    	$user = Auth::user();
    	$code = $request->input('code');

    	// check security code
    	$securityCode = DB::table('security_codes')->where('code', $code)->where('user_id', $user->id)->first();    	
    	if (is_null($securityCode)) {    		
    		return redirect()->back()->withErrors(['Invalid Code']);
    	}

    	DB::table('security_codes')
            ->where('user_id', $user->id)
            ->update(['is_used' => 1, 'updated_at' => Carbon::now()]);

        return redirect('/home');
    }
}

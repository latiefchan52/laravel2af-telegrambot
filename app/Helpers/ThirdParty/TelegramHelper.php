<?php

namespace App\Helpers\ThirdParty;

use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramHelper
{
    public function send($destination, $message)
    {
        Telegram::sendMessage([
            'chat_id' => $destination,
            'parse_mode' => 'HTML',
            'text' => '<b>Your Code Security : </b><code>' .$message.'</code>'            
        ]);
    }
}
